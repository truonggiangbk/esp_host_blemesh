#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "button.h"
#include "driver/gpio.h"

EventGroupHandle_t xCreatedEventGroup;

#define BIT_0    ( 1 << 0 )

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    BaseType_t xHigherPriorityTaskWoken, xResult;

    /* xHigherPriorityTaskWoken must be initialised to pdFALSE. */
    xHigherPriorityTaskWoken = pdFALSE;

    /* Set bit 0 and bit 4 in xEventGroup. */
    xResult = xEventGroupSetBitsFromISR(
                                xCreatedEventGroup,   /* The event group being updated. */
                                BIT_0 , /* The bits being set. */
                                &xHigherPriorityTaskWoken );
}

extern void bt_mesh_send_onoff(bool state);

static void gpio_task_example(void* arg)
{
    uint32_t io_num;
    for(;;) {
        xEventGroupWaitBits(
        xCreatedEventGroup,   /* The event group being tested. */
        BIT_0 , /* The bits within the event group to wait for. */
        pdTRUE,        /* BIT_0 & BIT_4 should be cleared before returning. */
        pdFALSE,       /* Don't wait for both bits, either bit will do. */
        portMAX_DELAY );/* Wait a maximum of 100ms for either bit to be set. */
        printf("Press\n");
        bt_mesh_send_onoff(1);
    }
}

void button_init(void)
{
    gpio_config_t io_conf;

    //interrupt of rising edge
    io_conf.intr_type = GPIO_PIN_INTR_NEGEDGE;
    //bit mask of the pins, use GPIO4/5 here
    io_conf.pin_bit_mask = (1<<GPIO_INPUT_IO_0);
    //set as input mode    
    io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);

    //create a queue to handle gpio event from isr
    xCreatedEventGroup = xEventGroupCreate();
    //start gpio task
    xTaskCreate(gpio_task_example, "gpio_task_example", 2048*2, NULL, 10, NULL);

    //install gpio isr service
    gpio_install_isr_service(0);
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_IO_0, gpio_isr_handler, (void*) GPIO_INPUT_IO_0);
}
